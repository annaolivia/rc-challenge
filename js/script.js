const titles = document.querySelectorAll('.article-item-title');
const infos = document.querySelectorAll('.article-item-productinfo');

titles.forEach(title => {
    removeTitle();
    title.classList.add('article-item-productinfo');
})

function removeTitle() {
    titles.forEach(title => {
        title.classList.remove('article-item-title');
    })
}