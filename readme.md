## Aufgabe:

**Manipulieren**: Verschiebe die Produktinfo in den Titel des Produktes und nutze dafür nur CSS/JS. Und ordne das Icon stimmig am rechten Rand des Titels.


**Responsive**: Passe die Liste der Artikel so an, dass sie in der Tablet / Phone View korrekt und stimmig angezeigt werden. Nutze hierfür CSS & Bootstrap Klassen (Plus: passe das Kachelgrid so an das es im IE 11 korrekt dargestellt wird).


**Kreativ**: Binde eine CSS oder Javascript Animation innerhalb der Artikel ein, die einen optischen Mehrwert schaffen, bei der Betrachtung und Nutzung des Shops.


**Hinzufügen**: Füge in das Layout einen Titelbereich (beinhaltet Bild / Titel Text / Kurzbeschreibung) ein. Dieser sollte Mobil wie auch im Desktop korrekt dargestellt werden.



## Hinweise:
Die offenen Dateien liegen im "src" Verzeichnis, diese kannst du erweitern oder auch neue Dateien in deinem Stil hinzufügen, fühle dich hier frei von Vorgaben. Als Kompilierung der Datei nutze ich hier "Grunt" um diesen Task zu starten, musst du auf das Hauptverzeichnis der Challenge, ein "npm install" laufen lassen. Sobald dies abgeschlossen ist,  kannst du diese  mit dem "Watch"-Task von Grunt Dateien bearbeiten und nach dem jeweiligen speichern von Dateien wird der Task automatisch ausgeführt. Die Bezeichnung des Tasks für das Terminal ist "grunt watch".
Solltest du weitere Fragen haben, kannst du mich gerne unter dk@simplydelivery.de erreichen.

### Weiterführende Links:

https://gruntjs.com/installing-grunt

https://getbootstrap.com/

https://fontawesome.com/?from=io 